Business Direct Open Source Directory Code

http://www.businessdirectmedia.org


Licensing 
---------

Please note that this Software is being distributed under the GNU General Public License and you will find a copy of the License Agreement in the Download Package.


Developers
----------
If you are a developer who would like to contribute to the Development of this Project, please visit the Project Website at http://www.businessdirectmedia.org for links to the different Communication Methods


